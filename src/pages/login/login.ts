import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading, AlertController, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { MyApp } from '../../app/app.component';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  myForm: FormGroup;
  user: Observable<firebase.User>;
  public loading:Loading;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private fb: Facebook,
    private auth: AuthServiceProvider,
    public events: Events
  ) {
    this.myForm = this.formBuilder.group({
      email: ['', Validators.pattern('^[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$')],
      password: ['', Validators.required]
    });
    this.user = afAuth.authState;
  }

  loginUser(){

   

    this.afAuth.auth.signInWithEmailAndPassword(this.myForm.value.email, this.myForm.value.password).then((user) => {
      // set info to app
      this.events.publish("user:login",user.user.uid);
      this.auth.setLogin(user.user.uid);
      this.navCtrl.setRoot("HomePage");
    }, (err) => {
      this.loading.dismiss().then( () => {
        let alert = this.alertCtrl.create({
          message: err.message,
          buttons: [
            {
              text: "Ok",
              role: 'cancel'
            }
          ]
        });
        alert.present();
      });
    });

    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
    });
    this.loading.present();
  }
  

  goToSignup(){
    this.navCtrl.push('SignupPage');
  }

  goToResetPassword(){
    this.navCtrl.push('ResetPasswordPage');
  }

  loginWithFacebook(): void {
    this.auth.loginWithFacebook().subscribe(user => {
      // set info to app
      this.events.publish("user:login",user.user.uid);
      this.auth.setLogin(user.user.uid);
      this.navCtrl.setRoot("HomePage");
    }, error=>{
      console.log(error);
    });
  }

}