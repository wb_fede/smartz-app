import { Component } from "@angular/core";
import { NavParams, NavController, DateTime } from 'ionic-angular';
import { CultivosService } from '../../providers/cultivos/cultivos';
import { AlertController } from 'ionic-angular';
import { DispositivosProvider } from '../../providers/dispositivos/dispositivos';
import { Equipos } from '../../model/equiposModel';
import { Response } from "@angular/http";
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

export class CultivoItem{
    id: number;
    tipo : String;
    dias_progreso: String;
    dias_totales: string;
    dispositivo_id: number;
    estado: number;
    fecha_fin: Date;
    fecha_inicio: Date;
    fecha_modificacion: Date;
    rutina_cultivo_id:number;
    nombre:string;
    nombre_rutina_cultivo:string;

}

export class RutinasItem{
    id: number;
    nombre : String;
    descripcion : String;
    estado: number;
    fecha_alta:DateTime;
    fecha_baja:DateTime;
    fecha_modificacion:DateTime;
}



// Navigation Details -> detalle cultivo
@Component({
    templateUrl: 'cultivos-details.html'
  })
export class NavigationDetailsPage {
    item;
  
    constructor(params: NavParams,public cultivosService:CultivosService,public nav: NavController) {
      this.item = params.data.item;
    }

    delete(){
       this.cultivosService.delete(this.item).subscribe((data:Response) => {
        this.cultivosService.set(null);
       });
    }
}

// Navigation Details -> Cultivo Personalizado
@Component({
    templateUrl: 'cultivos-personalizado.html'
  })
export class NavigationCultivoPersonalizado {
    data = {
        "nombre": "",
        "descripcion": "",
        "fasesRutinaCultivo": [
          {
            "duracion": 1,
            "horas_luz": 1,
            "fase_id": 1,
            "parametrosFaseCultivo": [
              {
                "valor_esperado": 1,
                "descripcion": "pHaceptable",
                "parametro_id": 1
              }
            ]
          },
          {
            "duracion": 1,
            "horas_luz": 1,
            "fase_id": 2,
            "parametrosFaseCultivo": [
              {
                "valor_esperado": 1,
                "descripcion": "pHaceptable",
                "parametro_id": 1
              }
            ]
          }
        ]
      };
  
    constructor(private userService:AuthServiceProvider, private cultivoService:CultivosService,public nav: NavController) {
      
    }

    save(){
        this.cultivoService.addPersonalizado(JSON.stringify(this.data),this.userService.currentUser).subscribe((data: Response) => {
            this.nav.pop()
        });
    }
}

// Page Cultivos
@Component({
    selector: 'page-cultivos',
    templateUrl: 'cultivos.component.html',
    entryComponents: [NavigationDetailsPage]
})

export class CultivosComponent{
    public cultivosList: CultivoItem[] = [];
    public rutinasList: RutinasItem[];
    public equipo: Equipos;
    public image;
    public imageDefault = "assets/imgs/hoja.svg";

    constructor(public nav: NavController, 
        public cultivosService:CultivosService,
        private alertCtrl: AlertController,
        private dispositivosService: DispositivosProvider,
        private userService:AuthServiceProvider){
        // obtengo id equipo
        this.equipo = dispositivosService.dispositivos[0];
        cultivosService.get(this.equipo.idCode).subscribe((response: CultivoItem) => {
            if(response[0] === undefined)
            {
                this.cultivosService.set(null);
            }else{
                this.cultivosService.set(response[0]);
                this.cultivosList[0] = this.cultivosService.cultivo;
                this.image = 'assets/imgs/' + this.cultivosList[0].nombre_rutina_cultivo + '.svg';
            }
            
        });
        
        
    }

    itemSelected(item){
        this.nav.push(NavigationDetailsPage, { item: item }).then(data => {
            this.cultivosList[0] = this.cultivosService.cultivo;
        });
    }

    createCultivo(){
        this.alertCtrl.create({
            title: 'Ingrese un nombre',
            inputs: [
              {
                name: 'nombre',
                placeholder: 'Nombre'
              }
            ],
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                handler: data => {
                }
              },
              {
                text: 'Guardar',
                handler: data => {
                }
              }
            ]
          });
    } 

    addCultivo(){
        //obtengo rutinas
        let rutinas = this.cultivosService.getRutina(this.userService.currentUser).subscribe((data:Response) => {

            let alert = this.alertCtrl.create({
                title: 'Elija tipo de cultivo',
                buttons: [
                  {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: data => {
                    }
                  },
                  {
                    text: 'Guardar',
                    handler: data => {
                      if(data == '100')
                      {
                        this.cultivoPersonalizado();
                      }else{
                        this.cultivosService.add(data,this.equipo.idCode).subscribe((data: CultivoItem) => {
                            this.cultivosList[0] = data;
                            this.image = 'assets/imgs/' + this.cultivosList[0].nombre_rutina_cultivo + '.svg';
                         });
                        }
                      
                    }
                  }
                ]
              });

            
            data['rutinas_cultivo'].forEach(element => {
                alert.addInput({
                    type: 'radio',
                    label: element.nombre,
                    value: element.id,
                    checked: false
                  });
            });
            alert.addInput({
                type: 'radio',
                label: 'Personalizado',
                value: '100',
                checked: false
              });
            
            alert.present();
        });
        
          
          
    }

    defaultUrl(){
        this.image = this.imageDefault;
    }

    cultivoPersonalizado(){
        this.nav.push(NavigationCultivoPersonalizado).then(data => {
            console.log(data);
        });
    }

}