import { Component } from "@angular/core";
import { DispositivosProvider } from '../../providers/dispositivos/dispositivos';
import { Time } from '@angular/common';
import { Equipos } from '../../model/equiposModel';
import { ConfiguracionesProvider } from '../../providers/configuraciones/configuraciones';
import { NetworkProvider } from '../../providers/network/network';
import { HotspotNetwork } from '@ionic-native/hotspot';
import { AlertController } from 'ionic-angular';

@Component({
    selector: 'page-configuraciones',
    templateUrl: 'configuraciones.component.html'
})

export class ConfiguracionesComponent{
    equipo:Equipos;
    cargando = false;
    statusWifi;
    dateIso;
    constructor(private dispositivoService:DispositivosProvider, private configuracionesService: ConfiguracionesProvider,
        private networkService:NetworkProvider, private alertCtrl:AlertController){
         this.equipo = dispositivoService.dispositivos[0];
         
         let date = this.equipo.fecha_cambio_filtro;
         let dateArray = date.split("/");

         this.dateIso = dateArray[2] + '-' + dateArray[1] + '-' + dateArray[0];

    }

    update(){
        // add form data
          let date = this.dateIso;
         let dateArray = date.split("-");

         this.equipo.fecha_cambio_filtro  = dateArray[2] + '/' + dateArray[1] + '/' + dateArray[0];
         //sistema apagado
        if(!this.equipo.estado)
        {
          this.equipo.notificaciones_on = 0;
          this.equipo.luz_on = 0;
          this.equipo.vaciar = 0;
        }
        this.configuracionesService.save(this.equipo.idCode,this.equipo).subscribe(
            (data: Equipos) => {
                this.equipo.estado = data.estado;
                this.equipo.hora_inicio = data.hora_inicio;
                this.equipo.notificaciones_on = data.notificaciones_on;
                this.equipo.luz_on = data.luz_on;
                this.equipo.vaciar = 0;
                console.log(data);
            }
        );

      }

      configurarWifi(){
        this.cargando = true;
        this.networkService.connectWifi().then((networks: Array<HotspotNetwork>) => {
            console.log(networks);
            let smartzWifi = networks.find((obj => {
              return obj.SSID === "SmartZ"
            }));
      
            if(smartzWifi === undefined)
            {
              this.cargando = false;
              this.presentAlert("Wifi no encontada","No se encuentra la red SmartZ. Revise que esté todo prendido");
            }else{
                // me conecto a la red
                console.log("Wifi encontrada");
                console.log(this.equipo.id);
                this.networkService.connectSmartz(this.equipo.id).then(data => {
                    
                    console.log("Me conecte al wifi");
                    console.log(data);
                    this.cargando = false;
                    console.log("envio data");
                this.getDataAlert();
                }).catch(data => {
                    this.presentAlert("Error","No pudimos conectarnos al wifi de Smartz");
                });
                
            }
        });;
      }
      presentAlert(title,text) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: text,
          buttons: ['Aceptar']
        });
        alert.present();
      }

      getDataAlert(){
        let alert = this.alertCtrl.create({
            title: 'Completa los datos de la red',
            inputs: [
                {
                  name: 'ssid',
                  placeholder: 'SSID'
                },
                {
                  name: 'password',
                  placeholder: 'Password',
                  type: 'password'
                }
              ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: data => {
                      
                    }
                  },
                  {
                    text: 'Conectar',
                    handler: data => {
                      this.networkService.configWifi(data.ssid,data.password).subscribe(data => {
                        console.log(data);
                          this.presentAlert("Conectado", "Equipo conectado correctamente");
                          this.cargando = false;
                      }, error =>
                      {
                        console.log(error);
                        this.presentAlert("Conectado", "Equipo conectado correctamente");
                      });
                    }
                  }
            ]
          });

    
          alert.present();
      }
}