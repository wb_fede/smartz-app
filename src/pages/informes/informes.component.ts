import { Component } from "@angular/core";
import { InformeModel } from "../../model/informe.model";
import { NavController, NavParams } from 'ionic-angular';
import { Tab1Page } from '../tab1/tab1';
import { TabPage } from '../tab/tab';
import { Tab2Page } from '../tab2/tab2';
@Component({
    selector: 'page-informes',
    templateUrl: 'informes.component.html'
})

export class InformesComponent{
    public condActuales: InformeModel[] = [];
    tab1Root = TabPage;
    tab2Root = Tab1Page;
    tab3Root = Tab2Page;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
}