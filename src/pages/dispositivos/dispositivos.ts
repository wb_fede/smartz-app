import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { DispositivosProvider } from '../../providers/dispositivos/dispositivos';

//model
import { Equipos } from '../../model/equiposModel';


@Component({
  templateUrl: 'dispositivos.html',
})
export class DispositivosPage {
  dispositivos: Equipos[] = [];
  callback: any;
  listView = true;
  currentUser:any;

  constructor(private auth: AuthServiceProvider,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private qrScanner: QRScanner,
    private alertCtrl: AlertController,
    public dispositivoService: DispositivosProvider) {

    // inicializo array
    if(navParams.get('dispositivos') != undefined)
    {
      this.dispositivos = navParams.get('dispositivos');
    }
   
   this.callback = this.navParams.get('callback');

   // get current user
    this.currentUser = this.auth.currentUser;

    //get dispositivos
  }

  save(equipo:Equipos){
    //enviar servicio
    
    if(equipo == null){
      console.log("error");
      return;
    }

    this.dispositivoService.add(equipo,this.currentUser).subscribe(data => {
      // save equipo
      this.dispositivos.push(equipo);
      this.callback(this.dispositivos).then( () => { this.navCtrl.pop() });
    }, 
    error => this.alert("Error","Hubo un error al guardar el dispositivo",false)); 

  }

  onSelectionChange(value,index){
    //invierto orden
    let aux = this.dispositivos[0];
    this.dispositivos[0] = value;
    this.dispositivos[index] = aux;
  }

  addQr(){
    // Optionally request the permission early
      this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        console.log("Scanning");
        if (status.authorized) {
          // camera permission was granted
          this.showCamera();
          this.qrScanner.show()
          console.log('Scan something');
          // start scanning
          let scanSub = this.qrScanner.scan().subscribe((text: string) => {
            console.log('Scanned something', text);
            this.hideCamera();
            //obtuve data
            let id = text['result'];
            console.log(id);
            // reviso si existe
            if(!this.dispositivos.find(i => i.id === id)){
              let equipo = new Equipos();
              
              //generate alert
              let alert = this.alertCtrl.create({
                title: 'Ingrese un nombre',
                inputs: [
                  {
                    name: 'nombre',
                    placeholder: 'Nombre'
                  }
                ],
                buttons: [
                  {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: data => {
                      console.log("El dispositivo ya existe");
                      this.presentAlert("El dispositivo ya existe","Equipo utilizado");
                    }
                  },
                  {
                    text: 'Guardar',
                    handler: data => {
                      console.log(data);
                      //reviso si es el primero
                      equipo.orden = this.dispositivos.length + 1;
                      equipo.nombre = data.nombre;
                      equipo.id = id;
                      //save in db
                      this.save(equipo);
                    }
                  }
                ]
              });
              this.hideCamera();
              alert.present();
            }
            this.qrScanner.hide(); // hide camera preview
            scanSub.unsubscribe(); // stop scanning
            
          });

        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
          this.hideCamera();
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
          this.hideCamera();
        }
      }).catch((e: any) => {console.log('Error is', e);this.hideCamera();});
  }

  presentAlert(text,title) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: ['Aceptar']
    });
    alert.present();
  }

  private showCamera() {
    ((<any>window).document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
    this.listView = false;
}
private hideCamera() {
    ((<any>window).document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
    this.listView = true;
}
ionViewDidLeave(){
  this.hideCamera();
}

alert(title,message,redirect) {
  let alert = this.alertCtrl.create({
    title: title,
    message: message,
    buttons: [
      {
        text: 'Aceptar',
        handler: () => {
        }
      }
    ]
  });
  alert.present();
}
}
