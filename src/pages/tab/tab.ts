import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { CultivosService } from '../../providers/cultivos/cultivos';
import { RutinasItem } from '../cultivos/cultimos.component';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the TabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab',
  templateUrl: 'tab.html',
})
export class TabPage {
  @ViewChild('barCanvas') barCanvas;
 
  barChart: any;

  constructor(private userService:AuthServiceProvider,public navCtrl: NavController, public navParams: NavParams, public cultivosService: CultivosService) {
  }

  ionViewDidLoad() {
    this.cultivosService.getRutina(this.userService.currentUser).subscribe((data: RutinasItem) =>{
        let labels: String[] = [];
        let dataLabels: String[] = [];
        data["rutinas_cultivo"].forEach(element => {
            labels.push(element.nombre);
            dataLabels.push(element.dias_totales);
        });

        this.barChart = new Chart(this.barCanvas.nativeElement, {

            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Tiempo',
                    data: dataLabels,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
    
        });
    });
    
  }
}
