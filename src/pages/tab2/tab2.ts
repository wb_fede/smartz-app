import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { ParametroProvider } from '../../providers/parametro/parametro';
import { DispositivosProvider } from '../../providers/dispositivos/dispositivos';
import { CultivosService } from '../../providers/cultivos/cultivos';

/**
 * Generated class for the Tab2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab2',
  templateUrl: 'tab2.html',
})
export class Tab2Page {
  @ViewChild('lineCanvas') lineCanvas;
  lineChart: any;
  parametros;
  parametroElegido = 1;
  tiempoElegido = 1;
  equipo;

  constructor(public navCtrl: NavController, public navParams: NavParams, private parametroService: ParametroProvider,
    private cultivoService:CultivosService) {
    this.parametroService.getParametros().subscribe(data => {
        this.parametros = data["parametros"];
        //dispositivo
        this.equipo = this.cultivoService.cultivo.id;
        //get data
        this.parametroService.getData(this.equipo,this.parametroElegido,this.tiempoElegido).subscribe(data => {
            console.log(data);
            this.generateChart(data[0],data[1]);
        });
    });
  }

  generateChart(labels,data) {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
 
      type: 'line',
      data: {
          labels: labels,
          datasets: [
              {
                  label: "Medición",
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(75,192,192,0.4)",
                  borderColor: "rgba(75,192,192,1)",
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: "rgba(75,192,192,1)",
                  pointBackgroundColor: "#fff",
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: "rgba(75,192,192,1)",
                  pointHoverBorderColor: "rgba(220,220,220,1)",
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  data: data,
                  spanGaps: false,
              }
          ]
      }

  });
  }

  getNewchart(){
      //get data
      this.parametroService.getData(this.equipo,this.parametroElegido,this.tiempoElegido).subscribe(data => {
        console.log(data);
        this.generateChart(data[0],data[1]);
    });
  }
}
