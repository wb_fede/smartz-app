import { Component } from "@angular/core";
import * as moment from 'moment';
import { ModalController, NavParams, NavController, DateTime } from 'ionic-angular';
import { NotificacionesProvider } from '../../providers/notificaciones/notificaciones';
import { DispositivosProvider } from '../../providers/dispositivos/dispositivos';

// Notificacion Item
export class NotificacionItem{
    tipo: number;
    asunto: String;
    mensaje: String;
    fecha: any;
    hora: String;

}

// modal
@Component({
    templateUrl: 'modalNotification.html'
})
export class ModalNotification {
    notificacion;
 constructor(params: NavParams) {
    this.notificacion = params.data.item;
 }

}

@Component({
    selector: 'page-notificaciones',
    templateUrl: 'notificaciones.component.html',
    entryComponents: [ModalNotification]
})

export class NotificacionesComponent{
    notificacionesHoy: NotificacionItem[] = [];
    notificacionesSemAnt: NotificacionItem[] = [];
    notificacionesAnteriores: NotificacionItem[] = [];
    alerta: String = 'assets/imgs/alert.svg';
    informacion: String = 'assets/imgs/advertencia.svg';

    public constructor(public nav: NavController, 
        private notificacionesServices:NotificacionesProvider,
        private dispositivosService: DispositivosProvider) {
        this.getNotificaciones();

        
    }


    getNotificaciones(){
        // test
        let notificaciones: NotificacionItem[] = [];
        //get Dispositivo
        let equipo = this.dispositivosService.dispositivos[0];
        //get notificaciones
        this.notificacionesServices.get(equipo.idCode,1).subscribe((response:Response) => {
            response['notificaciones'].forEach(element => {
                let notificacion = new NotificacionItem();
                notificacion.tipo = element['tipo_notificacion']['tipo'];
                notificacion.asunto = element['tipo_notificacion']['titulo'];
                notificacion.mensaje = element['tipo_notificacion']['mensaje'];
                notificacion.fecha =  new Date(element['fecha_alta']).toDateString();
                notificacion.hora =  new Date(element['fecha_alta']).toLocaleTimeString();
                notificaciones.push(notificacion);
            });
            // ordeno
            notificaciones = this.ordenarNotificaciones(notificaciones);
            // filtro
            this.dividirNotificaciones(notificaciones);
        });
    }

    ordenarNotificaciones(notificaciones){
        return notificaciones.sort((leftSide, rightSide): number => {
            if(leftSide.fecha < rightSide.fecha) return 1;
            if(leftSide.fecha > rightSide.fecha) return -1;
            return 0;
        });
    }

    dividirNotificaciones(notificaciones){
        let today = moment().format('YYYY/MM/DD');
        let week = moment().subtract(7,'days').format('YYYY/MM/DD');

        this.notificacionesHoy = notificaciones.filter(function(n){
            return moment(n.fecha).format('YYYY/MM/DD') == today;
        });

        this.notificacionesSemAnt = notificaciones.filter(function(n){
            n.fecha = moment(n.fecha).format('YYYY/MM/DD');
            return n.fecha < today && n.fecha > week;
        });

        this.notificacionesAnteriores = notificaciones.filter(function(n){
            n.fecha = moment(n.fecha).format('YYYY/MM/DD');
            return n.fecha < week;
        });
    }

    openModal(item) {
        this.nav.push(ModalNotification, { item: item });
      }

      doRefresh(refresher) {
        console.log('Begin async operation', refresher);
        this.getNotificaciones();
        setTimeout(() => {
          console.log('Async operation has ended');
          refresher.complete();
        }, 2000);
      }
}

