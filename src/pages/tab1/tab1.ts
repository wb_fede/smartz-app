import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { CultivosService } from '../../providers/cultivos/cultivos';

/**
 * Generated class for the Tab1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab1',
  templateUrl: 'tab1.html',
})
export class Tab1Page {
  @ViewChild('doughnutCanvas') doughnutCanvas;

  doughnutChart: any;
  cultivo;

  constructor(public navCtrl: NavController, public navParams: NavParams, private cultivosService:CultivosService) {
      this.cultivo = this.cultivosService.cultivo;
  }

  ionViewDidLoad() {

    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
        
      type: 'doughnut',
      data: {
          labels: ["Tiempo Transcurrido", "Tiempo Faltante"],
          datasets: [{
              label: '# of Votes',
              data: [this.cultivo.dias_progreso, this.cultivo.dias_totales],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              hoverBackgroundColor: [
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56",
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56"
              ]
          }]
      }

  });
  }

}
