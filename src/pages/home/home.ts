import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events } from 'ionic-angular';

//services
import { DispositivosProvider } from '../../providers/dispositivos/dispositivos';
import { CultivosService } from '../../providers/cultivos/cultivos';
import { LoginPage } from '../login/login';
import { CultivoItem } from '../cultivos/cultimos.component';
import { Mediciones, Equipos } from '../../model/equiposModel';
import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification';
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  public cultivoActual: CultivoItem;
  public image;
  public imageDefault = "assets/imgs/hoja.svg";
  public tempAgua:Mediciones = new Mediciones();
  public medicionAgua:Mediciones= new Mediciones();
  public tempAire:Mediciones= new Mediciones();
  public medicionPHMas:Mediciones= new Mediciones();
  public medicionPHMenos:Mediciones= new Mediciones();
  public medicionCE:Mediciones= new Mediciones();
  public medicionHumedad:Mediciones= new Mediciones();
  public medicionNutrienteA:Mediciones= new Mediciones();
  public medicionNutrienteB:Mediciones= new Mediciones();
  public medicionPh:Mediciones= new Mediciones();
  public diasProgeso:number;
  public diasTotales:number;
  public diasProgesoText: string;
  public diasTotalesText: string;
  public plantUrl: string = 'assets/imgs/plant.svg';
  public co2:Mediciones= new Mediciones();
  public dispositivoActual: Equipos;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public dispositivoService: DispositivosProvider,
    public cultivosService:CultivosService,
    public events: Events,
    private localNotification: PhonegapLocalNotification,
    private cultivoService: CultivosService
    ) {
      
      //obtengo dispositivo actual
      let dispositivoActual;
      if( this.dispositivoService.dispositivos != undefined && this.dispositivoService.dispositivos.length != 0)
      {
        dispositivoActual = this.dispositivoService.dispositivos[0];
        this.dispositivoActual = dispositivoActual;
        this.getDataCultivo(dispositivoActual);
      }
      // get user id
    events.subscribe('dispositivo:load', (data) => {
      let dispositivoActual;
      console.log(this.dispositivoService.dispositivos);
      if( this.dispositivoService.dispositivos != undefined && this.dispositivoService.dispositivos.length != 0)
      {
        dispositivoActual = this.dispositivoService.dispositivos[0];
        this.dispositivoActual = dispositivoActual;
        this.getDataCultivo(dispositivoActual);
      }
    });
  

  }

  ionViewDidLoad() {

  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getDataCultivo(this.dispositivoService.dispositivos[0]);
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  
  presentAlert(text,title) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: ['Aceptar']
    });

    alert.present();
  }


  alert(title,message,esperado) {
    console.log(message);
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
          }
        }
      ]
    });
    
    alert.present();
  }

  getDataCultivo(dispositivoActual)
  {
    // obtengo cultivos
    this.cultivosService.get(dispositivoActual.idCode).subscribe((data: CultivoItem) => {
      // no tiene dispositivos
        if(data[0] != undefined && data[0].length != 0){
          this.cultivosService.cultivo = data[0];
          this.cultivoActual = data[0];
          this.image = "assets/imgs/" + this.cultivoActual.nombre_rutina_cultivo + ".svg";
          //get mediciones
          console.log(this.cultivoActual);
          this.dispositivoService.getMediciones(this.cultivoActual).subscribe((data: Mediciones[]) => {
            if(data['mediciones'])
            {
              console.log(data['mediciones']);
              this.tempAgua = data["mediciones"]["Temp. Agua"];
              this.tempAire = data["mediciones"]["Temp. Ambiente"];
              this.medicionPHMas = data["mediciones"]["Nivel pH +"];
              this.medicionPHMenos = data["mediciones"]["Nivel pH -"];
              this.medicionCE = data["mediciones"]["CE"];
              this.medicionHumedad = data["mediciones"]["Humedad"];
              this.medicionNutrienteA = data["mediciones"]["Nivel Nutriente A"];
              this.medicionNutrienteB = data["mediciones"]["Nivel Nutriente B"];
              this.medicionPh = data["mediciones"]["pH"];
              this.co2 = data["mediciones"]["CO2"];

              this.diasProgeso = +this.cultivoActual.dias_progreso;
              this.diasTotales = +this.cultivoActual.dias_totales;

              let semanas = Math.floor(this.diasProgeso / 7);
              let dias = Math.floor(this.diasProgeso % 7);

              this.diasProgesoText = semanas.toString();  //+ ' semanas, ' + dias + ' días';

              semanas = Math.floor(this.diasTotales / 7);
              dias = Math.floor(this.diasTotales % 7);

              this.diasTotalesText = semanas.toString();  //+ ' semanas';

              //planta a mostrar
              let porcentaje = this.diasProgeso * 100 / this.diasTotales;
              if( porcentaje < 33){
                this.plantUrl = 'assets/imgs/plant.svg';
              }else{
                if( porcentaje < 66){
                  this.plantUrl = 'assets/imgs/plant1.svg';
                }else{
                  this.plantUrl = 'assets/imgs/plant2.svg';
                }
              }
            } 
            
          })
        }
    }, error => {
        this.alert("Error","No se pudo obtener información",false);
    } );

  }

  defaultUrl(){
    this.image = this.imageDefault;
}

  
}
