
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, DateTime, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AlertController } from 'ionic-angular';
//pages
import { HomePage } from '../pages/home/home';
import { NotificacionesComponent } from '../pages/notificaciones/notificaciones.component';
import { InformesComponent } from '../pages/informes/informes.component';
import { CultivosComponent } from '../pages/cultivos/cultimos.component';
import { ConfiguracionesComponent } from '../pages/configuraciones/configuraciones.component';
import { LoginPage } from '../pages/login/login';
import { DispositivosPage } from '../pages/dispositivos/dispositivos';

//services
import { DispositivosProvider } from '../providers/dispositivos/dispositivos';
import { Time } from '@angular/common';


// model
import { Equipos } from '../model/equiposModel';

// Background
import { BackgroundMode } from '@ionic-native/background-mode';
import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification';
import { NotificacionesProvider } from '../providers/notificaciones/notificaciones';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  userLogin: String;
  rootPage: any = LoginPage;
  dispositivos: Equipos[] = [];
  equipoActual:Equipos;
  countBack = 0;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen, 
    public dispositivoService: DispositivosProvider, 
    public events: Events,
    private alertCtrl: AlertController,
    private backgroundMode: BackgroundMode,
    private localNotification: PhonegapLocalNotification,
    private notificationService: NotificacionesProvider) {
    

    this.initializeApp();
    
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Inicio', component: HomePage },
      { title: 'Notificaciones', component: NotificacionesComponent },
      { title: 'Informes', component: InformesComponent },
      { title: 'Cultivos', component: CultivosComponent },
      { title: 'Configuraciones', component: ConfiguracionesComponent }
    ];

    // get user id
    events.subscribe('user:login', (data) => {
      this.userLogin = data;
      // get dispositivos
      this.dispositivoService.get(this.userLogin).subscribe((data: any[]) => {
          // no tiene dispositivos
          console.log(data);
          if(!data || data.length == 0 || data[0] == undefined)
          {
            this.alert("Información","No tienes dispositivos asociados. Escanea el QR de tu Smartz para comenzar",false)
          }else{
            data.forEach(item => {
              let newEquipo = new Equipos;
              newEquipo.orden = 0;
              // mapeo
              newEquipo = this.mapJson(newEquipo,item);

              //guardo
              this.dispositivos.push(newEquipo);
              //equipo actual
              this.equipoActual = newEquipo;
            });
            //save in service
            this.dispositivoService.dispositivos = this.dispositivos;
            this.events.publish("dispositivo:load");
          }
       }, error => {
         //para prod TRUE
          this.alert("Error","No se pudo obtener información",true);
       } );
       
    });
    
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.hideSplashScreen();
      this.statusBar.styleDefault();
      this.backgroundMode.enable();
      this.platform.registerBackButtonAction(() =>{
        console.log("BACK");
        if(this.countBack == 2){
          this.platform.exitApp();
        }else{
          this.countBack++;
          this.nav.setRoot(HomePage);
        }
      });
      
      
    });
    //notification
    setInterval(() => {
      if(this.dispositivoService.dispositivos[0] != undefined){
          this.notificationService.get(this.dispositivoService.dispositivos[0].idCode,0).subscribe((data) =>{
              data["notificaciones"].forEach(element => {
                console.log(element['tipo_notificacion']);
                this.notification(element['tipo_notificacion'].titulo,element['tipo_notificacion'].mensaje);
            });
          });
      } 
    }
        , 10000);
      
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logoutClicked() {
    this.nav.setRoot(LoginPage);
  }

  ordenarEquipos(){
      return this.dispositivos.sort((leftSide, rightSide): number => {
          if(leftSide.orden > rightSide.orden) return 1;
          if(leftSide.orden < rightSide.orden) return -1;
          return 0;
      });
  }

  openDispositivos(){
    this.nav.push(DispositivosPage, {dispositivos: this.dispositivos, callback: this.callbackDispositivo});
  }

  callbackDispositivo = callback =>
    {
      return new Promise((resolve, reject) => {
        this.dispositivos = callback;
        this.ordenarEquipos();
        this.equipoActual = callback[0];
        resolve();
      });
    };

    hideSplashScreen() {
        if (this.splashScreen) {
        setTimeout(() => {
          this.splashScreen.hide();
        }, 100);
        }
      }

    alert(title,message,redirect) {
        let alert = this.alertCtrl.create({
          title: title,
          message: message,
          buttons: [
            {
              text: 'Aceptar',
              handler: () => {
                if(redirect)
                {
                  this.logoutClicked();
                }else{
                  this.openDispositivos();
                }
              }
            }
          ]
        });
        alert.present();
      }


  //mapear json
  mapJson(destino: Equipos,origen){
    console.log(origen);
    destino.id = origen.codigo;
    destino.idCode = origen.id;
    destino.nombre = origen.descripcion;
    destino.estado = origen.estado;
    destino.fecha_alta = origen.fecha_alta;
    destino.fecha_baja = origen.fecha_baja;
    destino.fecha_modificacion = origen.fecha_modificacion;
    destino.fecha_vaciado = origen.fecha_vaciado;
    destino.hora_inicio = origen.hora_inicio;
    destino.luz_on = origen.luz_on;
    destino.notificaciones_on = origen.notificaciones_on;
    destino.vaciar = origen.vaciar;
    destino.orden = origen.orden;
    destino.fecha_cambio_filtro = origen.fecha_cambio_filtro;
    return destino;
  }

  notification(title,menssage){
    // Schedule a single notification
    this.localNotification.requestPermission().then(
      (permission) => {
        console.log(permission);
        if (permission === 'granted') {
    
          // Create the notification
          this.localNotification.create(title, {
            tag: title,
            body: menssage
          });
    
        }
      }
    );

  }
}
