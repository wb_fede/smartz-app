import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';


import { MyApp } from './app.component';

// Elements
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MomentModule } from 'angular2-moment';

// Pages
import { NotificacionesComponent, ModalNotification } from '../pages/notificaciones/notificaciones.component';
import { InformesComponent } from '../pages/informes/informes.component';
import { HomePage } from '../pages/home/home';
import { CultivosComponent, NavigationDetailsPage, NavigationCultivoPersonalizado } from '../pages/cultivos/cultimos.component';
import { ConfiguracionesComponent } from '../pages/configuraciones/configuraciones.component';
import { LoginPage } from '../pages/login/login';
import { DispositivosPage } from '../pages/dispositivos/dispositivos';
//Firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from 'angularfire2/auth';
export const firebaseConfig = {
  apiKey: "AIzaSyAst2AEsqsEZV9Wb57Co9rkTiQvY7TNpJQ",
  authDomain: "smartz-534af.firebaseapp.com",
  databaseURL: "https://smartz-534af.firebaseio.com",
  projectId: "smartz-534af",
  storageBucket: "smartz-534af.appspot.com",
  messagingSenderId: '108429024130'
};


//HTTTP
import { HTTP } from '@ionic-native/http';
import { HttpBackend, HttpXhrBackend } from '@angular/common/http';
import { NativeHttpModule, NativeHttpBackend, NativeHttpFallback } from 'ionic-native-http-connection-backend';
import { RequestOptions, Http } from '@angular/http';

// QR
import { QRScanner } from '@ionic-native/qr-scanner';
// Network
import { Network } from '@ionic-native/network';
import { Hotspot } from '@ionic-native/hotspot';
//facebook
import { Facebook } from '@ionic-native/facebook'
import { AuthServiceProvider } from '../providers/auth-service/auth-service';

//informes
import { TabPage } from '../pages/tab/tab';
import { Tab1Page } from '../pages/tab1/tab1';
import { Tab2Page } from '../pages/tab2/tab2';

//API
import { DispositivosProvider } from '../providers/dispositivos/dispositivos';

// Background
import { BackgroundMode } from '@ionic-native/background-mode';

// tooltip
import { CultivosService } from '../providers/cultivos/cultivos';
import { NotificacionesProvider } from '../providers/notificaciones/notificaciones';
import { ConfiguracionesProvider } from '../providers/configuraciones/configuraciones';
import { NetworkProvider } from '../providers/network/network';
import { ParametroProvider } from '../providers/parametro/parametro';

import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification';
@NgModule({
  declarations: [
    MyApp,
    NotificacionesComponent,
    InformesComponent,
    CultivosComponent,
    ConfiguracionesComponent,
    LoginPage,
    NavigationDetailsPage,
    NavigationCultivoPersonalizado,
    ModalNotification,
    DispositivosPage,
    TabPage,
    Tab1Page,
    Tab2Page
    ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    NativeHttpModule,
    MomentModule,
    BrowserAnimationsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    NotificacionesComponent,
    InformesComponent,
    CultivosComponent,
    ConfiguracionesComponent,
    LoginPage,
    NavigationDetailsPage,
    NavigationCultivoPersonalizado,
    ModalNotification,
    DispositivosPage,
    TabPage,
    Tab1Page,
    Tab2Page
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: Http, useClass: Http, deps: [NativeHttpFallback, RequestOptions]},
    QRScanner,
    Network,
    Hotspot,
    BackgroundMode,
    Facebook,
    AuthServiceProvider,
    DispositivosProvider,
    CultivosService,
    NotificacionesProvider,
    ConfiguracionesProvider,
    NetworkProvider,
    ParametroProvider,
    PhonegapLocalNotification

  ]
})
export class AppModule {}


