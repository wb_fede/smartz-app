import { DateTime } from "ionic-angular";
import { Time, DatePipe } from '@angular/common';

// Notificacion Item
export class Equipos{
    idCode:number; // id
    id:String; // codigo
    nombre:String; // descripcion
    estado:number;
    fecha_alta:DateTime;
    fecha_baja:DateTime;
    fecha_modificacion:DateTime;
    fecha_vaciado:DateTime;
    hora_inicio:number;
    orden:number;
    vaciar:number;
    notificaciones_on:number;
    luz_on:number;
    fecha_cambio_filtro:String;
  }


  export class Mediciones{
    nombre:string;
    valor:number;
    fecha:Date;
    parametro_id:number;

    Mediciones(){
      this.nombre = "";
      this.valor = 0;
      this.fecha = null;
      this.parametro_id = null;
    }
  }