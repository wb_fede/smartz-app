import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_API } from '../../global';
import { Equipos } from '../../model/equiposModel';

/*
  Generated class for the ConfiguracionesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConfiguracionesProvider {
  url_base:String;

  constructor(public http: HttpClient) {
    this.url_base = URL_API;
  }

  save(id,data:Equipos){
    if(data.vaciar){
      data.vaciar = 1;
    }else{
      data.vaciar = 0;
    }
    let header = new HttpHeaders();
    header.set('Content-Type','application/x-www-form-urlencoded');
    return this.http.put(this.url_base+'/dispositivos/editar/'+id,data, {headers:header}).timeout(5000);
  }

}
