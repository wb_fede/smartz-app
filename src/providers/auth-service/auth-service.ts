import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Facebook} from '@ionic-native/facebook';
import {auth} from 'firebase';
import {AngularFireAuth} from 'angularfire2/auth';
import {Platform} from 'ionic-angular';

export class User {
  name: string;
  email: string;
 
 
  constructor(name: string, email: string) {
    this.name = name;
    this.email = email;
  }
}

@Injectable()
export class AuthServiceProvider {
  currentUser: User;
  constructor(public http: HttpClient,private afAuth: AngularFireAuth, private fb: Facebook, private platform: Platform) {
    console.log('Hello AuthServiceProvider Provider');
  }
  
  public loginWithFacebook() {
    return Observable.create(observer => {
      if (this.platform.is('cordova')) {
        this.fb.login(['email', 'public_profile']).then(res => {
          const facebookCredential = auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
          this.afAuth.auth.signInAndRetrieveDataWithCredential(facebookCredential).then(user => {
            observer.next(user);
          }).catch(error => {
            observer.error(error);
          });
        }).catch((error) => {
          observer.error(error);
        });
      } else {
        this.afAuth.auth
          .signInWithPopup(new auth.FacebookAuthProvider())
          .then((res) => {
            observer.next(res);
          }).catch(error => {
          observer.error(error);
        });
      }
    });
  }

  setLogin(uid){
    this.currentUser = uid;
  }
}
