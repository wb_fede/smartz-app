import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_API } from '../../global';
import { CultivoItem } from '../../pages/cultivos/cultimos.component';

/*
  Generated class for the CultivosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CultivosService {
  url_base:String;
  cultivo:CultivoItem;
  
  constructor(public http: HttpClient) {
    this.url_base = URL_API;
  }

  get(id){
    return this.http.get(this.url_base+'/cultivos/'+id).timeout(5000);
  }
 
  getRutina(usuario)
  {
    return this.http.get(this.url_base+'/rutinas_cultivo/get/'+usuario).timeout(5000);
  }

  add(idRutina, idDispositivo){
    let formData: FormData = new FormData();

    formData.append('dispositivo_id', idDispositivo);
    formData.append('rutina_cultivo_id', idRutina);

    return this.http.post(this.url_base+'/cultivos/crear',formData).timeout(5000);
  }

  delete(cultivo:CultivoItem){
    let header = new HttpHeaders();
    cultivo.estado = 0;
    header.set('Content-Type','application/x-www-form-urlencoded');
    return this.http.put(this.url_base+'/cultivos/editar/'+cultivo.id,cultivo, {headers:header}).timeout(5000);
  }

  set(cultivo:CultivoItem){
    this.cultivo = cultivo;
  }

  addPersonalizado(rutina, usuario){
    let formData: FormData = new FormData();

    formData.append('rutina_cultivo', rutina);
    formData.append('codigo_usuario', usuario);

    return this.http.post(this.url_base+'/rutinas_cultivo/crear',formData).timeout(5000);
  }
}
