import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_API } from '../../global';
import { RequestOptions } from '@angular/http';
import 'rxjs/add/observable/empty' 
import { Equipos } from '../../model/equiposModel';
import { CultivoItem } from '../../pages/cultivos/cultimos.component';

/*
  Generated class for the DispositivosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DispositivosProvider {
  url_base:String;
  dispositivos: Equipos[] = [];

  constructor(public http: HttpClient) {
    this.url_base = URL_API;
  }

  get(user){
    console.log(user);
    return this.http.get(this.url_base+'/dispositivos/'+user).timeout(5000);
  }

  add(equipo:Equipos,usuario:string)
  {
    if(equipo == null)
    {
      return;
    }
    // add form data
    let formData: FormData = new FormData();

    formData.append('codigo', equipo.id.toString());
    formData.append('descripcion', equipo.nombre.toString());
    formData.append('usuario_id', usuario);

    return this.http.post(this.url_base+'/dispositivos/crear',formData).map((response: Response) => {
        return response;
    }).timeout(1000);  
  }

  update(equipo:Equipos){
    // add form data
    let formData: FormData = new FormData();

     
      for (var propiedad in equipo) {
        console.log(propiedad);
      }
  }

  getMediciones(equipo:CultivoItem){
    return this.http.get(this.url_base+'/mediciones/get/'+equipo.id).timeout(5000);
  }
}
