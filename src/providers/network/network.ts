import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { Hotspot, HotspotNetwork } from '@ionic-native/hotspot';
/*
  Generated class for the NetworkProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NetworkProvider {

  constructor(public http: HttpClient,private network: Network, private hotspot: Hotspot) {
   
  }

  connectWifi(){
    return this.hotspot.scanWifi();
  }

  configWifi(ssid,pass){
    let string = 'http://192.168.4.1/config?ssid='+ssid+'&pass='+pass;

    return this.http.get(string);
  }

  connectSmartz(pass){
    return this.hotspot.connectToWifi('SmartZ',pass);
  }

}
