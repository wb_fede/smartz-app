import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_API } from '../../global';
/*
  Generated class for the NotificacionesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotificacionesProvider {
  url_base:String;

  constructor(public http: HttpClient) {
    this.url_base = URL_API;
  }

  get(id,all){
    return this.http.get(this.url_base+'/notificaciones/get/'+id+'/'+all).timeout(5000);
  }
}
