import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_API } from '../../global';
/*
  Generated class for the ParametroProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ParametroProvider {
  public url_base;
  constructor(public http: HttpClient) {
    this.url_base = URL_API;
  }

  getParametros(){
    return this.http.get(this.url_base+'/parametros/get').timeout(5000);
  }

  getData(idcultivo,idparamentro,tiempo){
    /// mediciones/resporte/idcultivo/idparametro/tiempo (semanas)
    console.log(idcultivo+'-'+idparamentro+'-'+tiempo);
    return this.http.get(this.url_base+'/mediciones/reporte/'+ idcultivo + '/' + idparamentro + '/' + tiempo).timeout(5000);
  }

}
